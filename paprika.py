from creature import Creature
from gender_enum import GenderEnum
from adventure import Adventure
import colored
from colored import stylize


def main():
    print("Please enter your name: ")
    name = input()
    print("Gender: ")
    gender = ""

    while True:
        try:
            gender_input = input()
            gender = GenderEnum(gender_input.lower()).value
            break
        except:
            print("Try again. Male, female, pepper or dolphin: ")

    player = Creature(name, gender) # Namn och kön
    adventure = Adventure(player)

    if adventure.play():
        print(stylize("Congratulations! You've saved the world from a very, very very, very certain and deeply dark doom.", colored.fg("green")))
    else:
        print(stylize("\nThe world fall in to chaos and peppers eliminates all humans AND robots \
( they can no longer eliminate the humans for them as they would in the future, year 2000 ) :'(.", colored.fg("red")))
        
    print(stylize("\nThanks for playing! Hope you've enjoyed. Please play again!\n", colored.fg("yellow")))


if __name__ == "__main__":
   main()