from creature import Creature
import colored
from colored import stylize
import time
import random


class Adventure():
    def __init__(self, player):
        self.player = player
        self.enemy_pepper = Creature("Merlin", "Pepper")

    def play(self):
        if self.intro() is False:
            return

        return self.main_campaign()

    def main_campaign(self):
        print("You've entered a very very very dark dungeon. There's no light.")
        print("You can feel the hair in the back of your head raise and feel a stirring in the air.")

        print("What do you do: \
        \n\t1. Run and hide behind a rock. \
        \n\t2. You gather up your courage and fight blidly.")
        
        if self.request_player_choise("1", "2"):
            return self.run_and_hide()
        else:
            return self.fight_blindly()

    def run_and_hide(self):
        print(f"You see a silhouette of a {self.enemy_pepper.gender} walking towards the exit of the cave.")
        time.sleep(3)
        print("You can hear him grunt as he rolls closer. He has clearly weathered something.")
        time.sleep(3)
        print("It looks like he's facing you, you don't really know because of the dark dark daaark darkness in the very dark dark dark dungeon.")
        time.sleep(3)
        print("\nSQQQUEEEEEL SQQQUEEEEEL ERRRNEEERRRMYYYYY REEEEEEEEEEEEEEEE ERRRRNEEERMYY!!!")
        print(f"THE {self.enemy_pepper.gender.capitalize()} HAS SPOTTED YOU AND START CHANNELING HIS EBIL MIGHTY PEPPER SPRAY TOWARDS YOU!")
        time.sleep(1)
        print("\nWhat do you do? \
        \n\t1. You start to veva. Like a enraged windmill! \
        \n\t2. You lay down and play dead.")

        if self.request_player_choise("1", "2"):
            # Start to veva
            print("You channel your own inner windmill. The power of wind flows through you and the air starts to stirr around you while you arms, \
blazingly fast,\nrotates round and round and round and round. Very very VERY quick and dark as a bottomless pit.\n")
            time.sleep(3)
            print("You lash out on the enemy who's very very terrified of your whirlwindmillexecution move.")
            x = random.randint(1,10)
            time.sleep(3)
            if x >= 5:
                print(stylize("\n\tBAM! You veva the pepper to death\n", colored.fg("green")))
                time.sleep(3)
                return self.you_win()
            else:
                print(stylize(f"\n\tOopsie! You missed... You drunk? The ebil {self.enemy_pepper} killed you to death.\n", colored.fg("red")))
                return self.you_die()
        
        else:
            print(stylize("You lay down. Guess what happens? You get slain. Surpriced huh? You're not \"Hans Jørgen Olsen\" the 12 year old who \"feign death'd\" a MOOSE.", colored.fg("red")))
            self.you_die()

    @staticmethod
    def you_win():
        return True

    def you_die(self):
        print(stylize(f"You are deadedd by the evil pepper master {self.enemy_pepper}. Too bad. You where the last hope for the survival of the human race. Don't feel too bad. It's just a gem", colored.fg("red")))
        return False

    def fight_blindly(self):
        self.you_die()
        return False

    def intro(self):
            print(f"Welcome, {self.player} To the world of peppers")
            print("Are you brave enough to face peppers mutated into pigs? (yes/no)")
            return self.request_player_choise("yes", "no")
            
    @staticmethod
    def request_player_choise(first, second):
        answer = input()
        while answer.lower() not in (first, second):
            print(f"Sry. I didn't understand. {first} or {second}? ")
            answer = input()
        
        if answer.lower() == first.lower():
            return True
        else:
            return False

