from gender_enum import GenderEnum


class Creature:
    def __init__(self, name, gender):
        self.name = name
        try:
            self.gender = self.set_gender(gender)
        except:
            raise

        self.health_points = 20
        self.attack_damage = 5

    def set_gender(self, gender):
        for gen in GenderEnum:
            if gender.lower() == gen.value:
                return gender.lower()

    def __str__(self):
        return f"{self.name}"